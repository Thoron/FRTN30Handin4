function G = random_graph(k, nodes)

%Set number of nodes to start with
nodes_at_start = floor(k) + 1;

%Create the adjecancy matrix for a complete graph
W = ~diag(ones(1, nodes_at_start));

%Get the average number of links to place
c = k/2;

%Start generate nodes
for t = 1 : nodes - nodes_at_start
    
    k0 = length(W); %Update size of W
    w = sum(W,2);   %Calculate out degree vector
    P = w./sum(w);  %Update probabilities
    
    %Calculate number of links to be added
    links = floor(c) + uint32(rand < (c - floor(c)));
    
    %Add each link
    for j = 1 : links
        %Calculate a neighbor to node
        neighbor = randsample(k0, 1, true, P);
        %Make sure not to draw same neighbor
        P(neighbor) = 0;
        %Update W-matrix
        W(k0+1,neighbor) = 1;
        W(neighbor,k0+1) = 1;
    end    
end
%Create graph and return 
G = graph(W);
end