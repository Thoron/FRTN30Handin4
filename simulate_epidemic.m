function [s, i, r, ipw] = simulate_epidemic(G, infect_init, beta, rho, sim_time, vacc)

if nargin < 6
    vacc = zeros(1, sim_time + 1);
else
    if length(vacc) ~= sim_time
        error('Vaccination vector must have a length corresponding to sim_time.')
    else
        vacc(end + 1) = vacc(end);
    end
end

%Get number of nodes in graph
n = numnodes(G);

%State vector
%S is 0
%I is 1
%R is 2
X = zeros(n, 1);

%Vaccine vector
%Not vaccinated is 0
%Vaccinated is 1
V = zeros(n, 1);

%Set some nodes to start as infected
X(randperm(n, infect_init), 1) = 1;

%Check if some individuals should be vaccinated at start
%In that case select some at random
V(randsample(n, round(vacc(1)*n/100))) = 1;

%Pre-allocated vectors
susceptible = zeros(sim_time,1);
infected = zeros(sim_time,1);
recovered = zeros(sim_time,1);
infected_per_week = zeros(sim_time, 1);

%Set initial values
susceptible(1) = n - infect_init;
infected(1) = infect_init;
recovered(1) = 0;
infected_per_week(1) = infect_init;

%Start simulation (one week at the time)
for t = 2 : sim_time + 1
    
    %Get fraction of individuals to be vaccined
    frac_vacc = (vacc(t)-vacc(t - 1))/100;
    %Calculate how many individuals that is
    vacc_ind = round(frac_vacc*n);
    %Select individuals at random to vaccine
    V(randsample(find(V == 0), vacc_ind)) = 1;
    
    %Set these to zero at the beginning of every week
    Xt_next = zeros(n, 1);
    infected_t = 0;
    
    %Go thorugh every individual and determine their next state
    for i = 1 : n
        
        %%%Check if individual is susceptible and not vaccinated
        if X(i) == 0 && V(i) == 0
            %Get neighbors
            neigh = neighbors(G, i);
            %Calculate how many that is infected and not vaccinated
            m = sum(X(neigh) == 1 & V(neigh) == 0);
            %Select next state at random
            Xt_next(i) = rand < (1 - (1 - beta)^m);
            %Update that one individual got infected
            infected_t = infected_t + Xt_next(i);
            
        %%% Check if individual is infected   
        elseif X(i) == 1
            %Select next state at random
            Xt_next(i) = (rand < rho) + 1;
        
        %%%Check if individual has recovered
        elseif X(i) == 2
            %Do nothing
            Xt_next(i) = 2;
        end
        %Else if individual is susceptible and vaccinated, do nothing
    end
    
    %Update X to current data
    X = Xt_next;
    
    %Save all the statistics from the week
    susceptible(t) = sum(X == 0);
    infected(t) = sum(X == 1);
    recovered(t) = sum(X == 2);
    infected_per_week(t) = infected_t;
end

%Assign return values
s = susceptible;
i = infected;
r = recovered;
ipw = infected_per_week;
end

