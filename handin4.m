%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%   Hand-in 4 FRTN30   %%%%%%
%%%%%%   By Fabian Aagren   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%
%%%% 1.1  %%%%
%%%%%%%%%%%%%%

clc
clear all
close all

%%%Set parameters
n = 500;           %Number of nodes in graph
infect_init = 10;  %Infected individuals at start
beta = .3;         %Infection probability
rho = .7;          %Recovery probability
sim_time = 15;     %Length of simulation in weeks
N = 100;           %Simulation will be run N times 

%Set up the adjacency matrix
W = zeros(n);
W = W + diag(ones(n-1,1),1);
W = W + diag(ones(n-1,1),-1);
W = W + diag(ones(n-2,1),2);
W = W + diag(ones(n-2,1),-2);
W = W + diag(ones(1,1),n-1);
W = W + diag(ones(1,1),1-n);
W = W + diag(ones(2,1),n-2);
W = W + diag(ones(2,1),2-n);
W = sparse(W);

%Create graph
G = graph(W);

%Pre-allocate variables
s = zeros(N, sim_time + 1);
r = s;
i = s;
ipw = s;

%Start simulation
for j = 1 : N
    [s(j, :), i(j, :), r(j, :), ipw(j, :)] = simulate_epidemic(G, infect_init, beta, rho, sim_time);
end

%Calculate means
s = mean(s);
i = mean(i);
r = mean(r);
ipw = mean(ipw);

%Plot average simulation result
plot_simulation(s, i, r, ipw)

%%

%%%%%%%%%%%%%%
%%%% 1.2  %%%%
%%%%%%%%%%%%%%

clear all
clc
close all

%Set average degree
k = 5.3;
%The total number of node in generated graph
nodes = 900;

%Generate the graph
G = random_graph(k, nodes);

%Calculate mean degree and display
mean_degree = mean(degree(G));
disp(['The mean degree of the graph is ' num2str(mean_degree)])


%%

%%%%%%%%%%%%%%
%%%%   2  %%%%
%%%%%%%%%%%%%%

clear all
clc
close all

%Set parameters 
k = 6;              %Average degree in graph
nodes = 500;        %Total number of nodes in graph
infect_init = 10;   %Individuals to be infected at start
beta = .3;          %Infection probability
rho = .7;           %Recovery probability
sim_time = 15;      %Length of simulation in weeks
N = 100;            %Simulation will be run N times

%Generate graph
G = random_graph(k, nodes);

%Pre-allocate variables
s = zeros(N, sim_time + 1);
r = s;
i = s;
ipw = s;

%Start simulation
for j = 1 : N
    [s(j, :), i(j, :), r(j, :), ipw(j, :)] = simulate_epidemic(G, infect_init, beta, rho, sim_time);
end

%Calculate means
s = mean(s);
i = mean(i);
r = mean(r);
ipw = mean(ipw);

%Plot average simulation result
plot_simulation(s, i, r, ipw)
%%

%%%%%%%%%%%%%%
%%%%   3  %%%%
%%%%%%%%%%%%%%

clear all
clc
close all

%Set parameters 
k = 6;              %Average degree in graph
nodes = 500;        %Total number of nodes in graph
infect_init = 10;   %Individuals to be infected at start
beta = .3;          %Infection probability
rho = .7;           %Recovery probability
sim_time = 15;      %Length of simulation in weeks
N = 100;            %Simulation will be run N times

%Vaccine per week
vacc = [0 5 15 25 35 45 55 60 60 60 60 60 60 60 60];

%Generate graph
G = random_graph(k, nodes);

%Pre-allocate variables
s = zeros(N, sim_time + 1);
r = s;
i = s;
ipw = s;

%Start simulation
for j = 1 : N
    [s(j, :), i(j, :), r(j, :), ipw(j, :)] = simulate_epidemic(G, infect_init, beta, rho, sim_time, vacc);
end

%Calculate means
s = mean(s);
i = mean(i);
r = mean(r);
ipw = mean(ipw);

%Plot average simulation result
plot_simulation(s, i, r, ipw)

%%

%%%%%%%%%%%%%%
%%%%   4  %%%%
%%%%%%%%%%%%%%

clc
clear all
close all

%Set parameters
nodes = 934;        %Number of nodes in graph
N = 10;             %Run simulation N times
infect_init = 1;    %Infected individuals at start
sim_time = 15;      %Simulation time in weeks

%Vaccine vector
vacc = [5 9 16 24 32 40 47 54 59 60 60 60 60 60 60];

%True vector for newly infected each week
I0 = [1 3 5 9 17 32 32 17 5 2 1 0 0 0 0];

%Set an initial rmse
min_RMSE = realmax;

%Initial guesses
delta_k = .1;
delta_beta = .05;
delta_rho = .05;

k0 = 6.7;
beta0 = .3;
rho0 = .6;

%Start algorithm
while true
    
    %Iterate through the parameter set of k
    for k = k0 - delta_k : delta_k : k0 + delta_k
        
        %Create a random graph
        G = random_graph(k, nodes);
        
        %Iterate through the parameter set of beta
        for beta = beta0 - delta_beta : delta_beta : beta0 + delta_beta
            
            %Iterate through the parameter set of rho
            for rho = rho0 - delta_rho : delta_rho : rho0 + delta_rho
                
                %Preallocate vector
                ipwt = zeros(sim_time + 1, 1);
                
                %Simulate the epidemic N times
                for i = 1 : N
                    [~, ~, ~, ipw] = simulate_epidemic(G, infect_init, beta, rho, sim_time, vacc);
                    %Add every result to a total
                    ipwt = ipwt + ipw;
                end
                %Calculate the average result
                I = ipwt/N;
                %Calculate root mean square error
                RMSE = sqrt(mean((I(2:end) - I0').^2));
                %Save parameters
                param = [k, beta, rho];
                
                %Check if a new minimum is found
                if RMSE < min_RMSE
                    %Update new minimum
                    min_RMSE = RMSE;
                    %Save params
                    min_param = param;
                    
                    %Plot new minimum 
                    t = 0:15;
                    plot(t, I)
                    hold on
                    plot(t, [1 I0])
                    drawnow
                end
            end
        end
    end
    
    %If same parameters is found stop algorithm
    if isequal(min_param, [k0 beta0 rho0])
        break
    end
    
    %Update new best found parameters
    k0 = min_param(1);
    beta0 = min_param(2);
    rho0 = min_param(3);
end

disp('Algorithm done!')
disp(' ')
disp('Best parameters are')
disp(num2str(min_param))
disp(' ')
%%
N = 100;

%Generate random graph
G = random_graph(k0, nodes);

%Pre-allocate variables
s = zeros(N, sim_time + 1);
r = s;
i = s;
ipw = s;

%Start simulation
for j = 1 : N
    [s(j, :), i(j, :), r(j, :), ipw(j, :)] = simulate_epidemic(G, infect_init, beta0, rho0, sim_time, vacc);
end

%Calculate means
s = mean(s);
i = mean(i);
r = mean(r);
ipw = mean(ipw);

%Plot average simulation result
plot_simulation(s, i, r, ipw)
