function plot_simulation(s, i, r, ipw)

t = 0 : length(s) - 1;
figure
subplot(311)
plot(t, s)
title('Susceptible individuals')
xlabel('week')
ylabel('individuals')
subplot(312)
plot(t, i)
title('Infected individuals')
xlabel('week')
ylabel('individuals')
subplot(313)
plot(t, r)
title('Recovered individuals')
xlabel('week')
ylabel('individuals')

figure
plot(t, ipw)
title('Newly infected individuals')
xlabel('week')
ylabel('individuals')

end